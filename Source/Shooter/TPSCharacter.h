// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATPSCharacter();

	// Called every frame.




	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() const { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;



public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION()
		void InputAxisX(float Value);

	UFUNCTION()
		void InputAxisY(float Value);

	UFUNCTION()
		void InputSprint(float Value);

	UFUNCTION()
		void InputWalk(float Value);

	UFUNCTION()
		void InputAim(float Value);

	UFUNCTION()
		void InputFire(float Value);

	float ValueAim = 0;
	float ValueWalk = 0;
	float ValueSprint = 0;
	float ValueX = 0;
	float ValueY = 0;

	int SumStand = 0;
	int SumWalk = 0;
	int SumSprint = 0;

	float LeftLimit = -1875.0f;
	float RightLimit = 1875.0f;
	float TopLimit = 1115.0f;
	float DownLimit = -1635.0f;
	float HighLimit = 170.0f;

	float ResSpeed = 600;

	int SumRunAnimation = 0;
	int SumAimAnimation = 0;
	int SumCrouchAnimation = 0;

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);

	UFUNCTION()
		void Cursor();

	UFUNCTION(BlueprintNativeEvent)
		void Fire(int AnimNum);
	void Fire_Implementation(int AnimNum);

};