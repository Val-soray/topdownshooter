
#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	CameraBoom->CameraLagSpeed = 5;

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MovementTick(DeltaTime);

	Cursor();
}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);

	NewInputComponent->BindAxis(TEXT("Sprint"), this, &ATPSCharacter::InputSprint);

	NewInputComponent->BindAxis(TEXT("Walk"), this, &ATPSCharacter::InputWalk);

	NewInputComponent->BindAxis(TEXT("Aim"), this, &ATPSCharacter::InputAim);

	NewInputComponent->BindAxis(TEXT("Fire"), this, &ATPSCharacter::InputFire);
}

void ATPSCharacter::InputAxisX(float Value)
{
	ValueX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	ValueY = Value;
}

void ATPSCharacter::CharacterUpdate()
{
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	case EMovementState::WalkAim_State:
		ResSpeed = MovementInfo.WalkAimSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPSCharacter::InputSprint(float Value)
{
	ValueSprint = Value;
	if (ValueSprint == 1)
	{
		if (SumSprint == 0)
		{
			Fire(-1);
			Fire(-2);
			Fire(-3);
			SumRunAnimation = 0;
			SumAimAnimation = 0;
			SumCrouchAnimation = 0;
			SumSprint = 1;
			SumWalk = 0;
			ValueSprint = 0;
			ChangeMovementState(EMovementState::Sprint_State);
		}
		if (SumSprint == 2)
		{

			SumSprint = 3;
		}
	}
	else if (ValueSprint == 0)
	{
		if (SumSprint == 1)
		{
			SumSprint = 2;
		}
		else if (SumSprint == 3)
		{
			SumSprint = 0;
			ChangeMovementState(EMovementState::Run_State);
		}
	}
}

void ATPSCharacter::InputWalk(float Value)
{
	ValueWalk = Value;
	if (ValueWalk == 1)
	{
		if(SumWalk == 0)
		{
			SumWalk = 1;
			SumSprint = 0;
			ValueSprint = 0;
			ChangeMovementState(EMovementState::Walk_State);
		}
		if (SumWalk == 2)
		{

			SumWalk = 3;
		}
	}
	else if (ValueWalk == 0)
	{
		if (SumWalk == 1)
		{
			SumWalk = 2;
		}
		else if (SumWalk == 3)
		{
			SumWalk = 0;
			ChangeMovementState(EMovementState::Run_State);
		}
	}

}

void ATPSCharacter::InputAim(float Value)
{
	ValueAim = Value;
	if (ValueAim == 1)
	{
		SumSprint = 0;
		if (SumWalk != 0)
		{
			ChangeMovementState(EMovementState::WalkAim_State);
		}
		else
		{
			ChangeMovementState(EMovementState::Aim_State);
		}
	}
	else
	{
		if (SumWalk != 0)
		{
			ChangeMovementState(EMovementState::Walk_State);
		}
		else if (SumSprint != 0)
		{
			ChangeMovementState(EMovementState::Sprint_State);
		}
		else
		{
			ChangeMovementState(EMovementState::Run_State);
		}
	}

}

void ATPSCharacter::InputFire(float Value)
{	
	if(Value == 1)
	{
		if (SumWalk == 0 && ValueAim == 0 && ValueX != 0 || SumWalk == 0 && ValueAim == 0 && ValueY != 0 || SumSprint != 0)
		{
			if (SumRunAnimation == 0)
			{
				ChangeMovementState(EMovementState::Run_State);
				SumSprint = 0;
				SumRunAnimation = 1;
				SumAimAnimation = 0;
				SumCrouchAnimation = 0;
				Fire(1);
			}

		}
		if (ValueAim == 1 && SumSprint == 0 && SumWalk == 0 && ValueX != 0 || ValueAim == 1 && SumSprint == 0 && SumWalk == 0 && ValueY != 0)
		{
			if (SumAimAnimation == 0)
			{
				SumAimAnimation = 1;
				SumRunAnimation = 0;
				SumCrouchAnimation = 0;
				Fire(2);
			}

		}
		if (SumWalk != 0 && SumSprint == 0 || ValueX == 0 && ValueY == 0)
		{
			if (SumCrouchAnimation == 0)
			{
				SumCrouchAnimation = 1;
				SumRunAnimation = 0;
				SumAimAnimation = 0;
				Fire(3);
			}
		}
	}
	else if (Value == 0)
	{
		if (SumSprint == 0 && SumWalk == 0 && ValueAim == 0 && ValueX != 0 || SumSprint == 0 && SumWalk == 0 && ValueAim == 0 && ValueY != 0)
		{
			if (SumRunAnimation == 1)
			{
				SumRunAnimation = 0;
				Fire(-1);
			}
		}
		if (ValueAim == 1 && SumSprint == 0 && SumWalk == 0 && ValueX != 0 || ValueAim == 1 && SumSprint == 0 && SumWalk == 0 && ValueY != 0)
		{
			if (SumAimAnimation == 1)
			{
				SumAimAnimation = 0;
				Fire(-2);
			}
		}
		if (SumWalk != 0 && SumSprint == 0 || ValueX == 0 && ValueY == 0)
		{
			if(SumCrouchAnimation == 1)
			{
				SumCrouchAnimation = 0;
				Fire(-3);
			}
		}
	}
}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

void ATPSCharacter::MovementTick(float DeltaTime)
{	
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), ValueX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), ValueY);

	if(SumSprint == 0)
	{
		APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (MyController)
		{
			FHitResult ResultHit;
			MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
			float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
		}
	}
	else
	{
		if (ValueX == 0 && ValueY == 0)
		{
			ChangeMovementState(EMovementState::Run_State);
			SumSprint = 0;
		}
	}
}

void ATPSCharacter::Cursor()
{
	
	if (CursorToWorld != nullptr)
	{
		//if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			//���� ������ �� ����� �� �������:
			if (TraceHitResult.Location.Y < RightLimit && TraceHitResult.Location.X < TopLimit && TraceHitResult.Location.Y >LeftLimit && TraceHitResult.Location.X > DownLimit)
			{
				CursorToWorld->SetWorldLocation(TraceHitResult.Location);
				CursorToWorld->SetWorldRotation(CursorR);
			}
			//���� ������ �����: 
		    //������:
			else if (TraceHitResult.Location.X >= TopLimit && TraceHitResult.Location.Y < RightLimit && TraceHitResult.Location.Y > LeftLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(TopLimit, TraceHitResult.Location.Y, HighLimit), true, nullptr, ETeleportType(false));
			}
			//����:
			else if (TraceHitResult.Location.X <= DownLimit && TraceHitResult.Location.Y < RightLimit && TraceHitResult.Location.Y > LeftLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(DownLimit, TraceHitResult.Location.Y, HighLimit), true, nullptr, ETeleportType(false));
			}
			//������:
			else if (TraceHitResult.Location.Y >= RightLimit && TraceHitResult.Location.X < TopLimit && TraceHitResult.Location.X > DownLimit && TraceHitResult.Location.X < TopLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(TraceHitResult.Location.X, RightLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
			//�����:
			else if (TraceHitResult.Location.Y <= LeftLimit && TraceHitResult.Location.X > DownLimit && TraceHitResult.Location.X > DownLimit && TraceHitResult.Location.X < TopLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(TraceHitResult.Location.X, LeftLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
			//�� ������ ����:
			else if (TraceHitResult.Location.X >= TopLimit && TraceHitResult.Location.Y >= RightLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(TopLimit, RightLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
			else if (TraceHitResult.Location.X >= TopLimit && TraceHitResult.Location.Y <= LeftLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(TopLimit, LeftLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
			//�� ������ ����:
			else if (TraceHitResult.Location.X <= DownLimit && TraceHitResult.Location.Y >= RightLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(DownLimit, RightLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
			else if (TraceHitResult.Location.X <= DownLimit && TraceHitResult.Location.Y <= LeftLimit)
			{
				CursorToWorld->SetWorldLocation(FVector(DownLimit, LeftLimit, HighLimit), true, nullptr, ETeleportType(false));
			}
		}
	}
}

void ATPSCharacter::Fire_Implementation(int AnimNum){}
