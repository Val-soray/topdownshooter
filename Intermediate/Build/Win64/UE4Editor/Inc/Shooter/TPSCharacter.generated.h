// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EMovementState : uint8;
#ifdef SHOOTER_TPSCharacter_generated_h
#error "TPSCharacter.generated.h already included, missing '#pragma once' in TPSCharacter.h"
#endif
#define SHOOTER_TPSCharacter_generated_h

#define Shooter_Source_Shooter_TPSCharacter_h_13_SPARSE_DATA
#define Shooter_Source_Shooter_TPSCharacter_h_13_RPC_WRAPPERS \
	virtual void Fire_Implementation(int32 AnimNum); \
 \
	DECLARE_FUNCTION(execFire); \
	DECLARE_FUNCTION(execCursor); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execInputFire); \
	DECLARE_FUNCTION(execInputAim); \
	DECLARE_FUNCTION(execInputWalk); \
	DECLARE_FUNCTION(execInputSprint); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execMovementTick);


#define Shooter_Source_Shooter_TPSCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFire); \
	DECLARE_FUNCTION(execCursor); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execInputFire); \
	DECLARE_FUNCTION(execInputAim); \
	DECLARE_FUNCTION(execInputWalk); \
	DECLARE_FUNCTION(execInputSprint); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX); \
	DECLARE_FUNCTION(execMovementTick);


#define Shooter_Source_Shooter_TPSCharacter_h_13_EVENT_PARMS \
	struct TPSCharacter_eventFire_Parms \
	{ \
		int32 AnimNum; \
	};


#define Shooter_Source_Shooter_TPSCharacter_h_13_CALLBACK_WRAPPERS
#define Shooter_Source_Shooter_TPSCharacter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATPSCharacter(); \
	friend struct Z_Construct_UClass_ATPSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Shooter"), NO_API) \
	DECLARE_SERIALIZER(ATPSCharacter)


#define Shooter_Source_Shooter_TPSCharacter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesATPSCharacter(); \
	friend struct Z_Construct_UClass_ATPSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Shooter"), NO_API) \
	DECLARE_SERIALIZER(ATPSCharacter)


#define Shooter_Source_Shooter_TPSCharacter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATPSCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATPSCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPSCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPSCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPSCharacter(ATPSCharacter&&); \
	NO_API ATPSCharacter(const ATPSCharacter&); \
public:


#define Shooter_Source_Shooter_TPSCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATPSCharacter(ATPSCharacter&&); \
	NO_API ATPSCharacter(const ATPSCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATPSCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATPSCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATPSCharacter)


#define Shooter_Source_Shooter_TPSCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATPSCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATPSCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__CursorToWorld() { return STRUCT_OFFSET(ATPSCharacter, CursorToWorld); }


#define Shooter_Source_Shooter_TPSCharacter_h_10_PROLOG \
	Shooter_Source_Shooter_TPSCharacter_h_13_EVENT_PARMS


#define Shooter_Source_Shooter_TPSCharacter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Shooter_Source_Shooter_TPSCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Shooter_Source_Shooter_TPSCharacter_h_13_SPARSE_DATA \
	Shooter_Source_Shooter_TPSCharacter_h_13_RPC_WRAPPERS \
	Shooter_Source_Shooter_TPSCharacter_h_13_CALLBACK_WRAPPERS \
	Shooter_Source_Shooter_TPSCharacter_h_13_INCLASS \
	Shooter_Source_Shooter_TPSCharacter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Shooter_Source_Shooter_TPSCharacter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Shooter_Source_Shooter_TPSCharacter_h_13_PRIVATE_PROPERTY_OFFSET \
	Shooter_Source_Shooter_TPSCharacter_h_13_SPARSE_DATA \
	Shooter_Source_Shooter_TPSCharacter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Shooter_Source_Shooter_TPSCharacter_h_13_CALLBACK_WRAPPERS \
	Shooter_Source_Shooter_TPSCharacter_h_13_INCLASS_NO_PURE_DECLS \
	Shooter_Source_Shooter_TPSCharacter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOOTER_API UClass* StaticClass<class ATPSCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Shooter_Source_Shooter_TPSCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
